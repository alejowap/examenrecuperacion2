var imcTotal = 0;
var numRegistros = 0;
var registros = document.getElementById("registros");

var txtEdad = document.getElementById("txtEdad");
var txtAltura = document.getElementById("txtAltura");
var txtPeso = document.getElementById("txtPeso");
var txtimc = document.getElementById("txtimc");
var txtNivel = document.getElementById("txtNivel");

function generar() { // Generar valores aleatorios
	txtEdad.value = Math.floor(Math.random() * (99 - 18) + 18);
	txtAltura.value = (Math.random() * (2.5 - 1.5) + 1.5).toFixed(2);
	txtPeso.value = (Math.random() * (130 - 20) + 20).toFixed(2);
	txtimc.value = "";
	txtNivel.value = "";
}

function calcular() { // Calcular imc a partir de altura y peso y ademas determinar nivel
	var altura = txtAltura.value;
	var peso = txtPeso.value;
	var imc = peso / Math.pow(altura, 2);

	if (imc < 18.5) {
		txtNivel.value = "Bajo peso";
	} else if (imc >= 18.5 && imc <= 24.9) {
		txtNivel.value = "Peso Saludable";
	} else if (imc >= 25.0 && imc <= 29.9) {
		txtNivel.value = "Sobrepeso";
	} else if (imc > 30.0) {
		txtNivel.value = "Obesidad";
	} else {
		return alert("Introduzca bien los datos");
	}

	txtimc.value = imc.toFixed(2);
}

function registrar() { // Agregar los 
	if ( txtEdad.value === "" || txtAltura.value === "" || txtPeso.value === "" || txtimc.value === "" || txtNivel.value === "") {
		return alert("Rellene los campos faltantes");
	}
	
	var imcConvertido = parseFloat(txtimc.value);
	
	numRegistros = numRegistros + 1;
	imcTotal = imcTotal + imcConvertido;
	document.getElementById("promedioIMC").innerHTML = (imcTotal / numRegistros).toFixed(2);
	registros.innerHTML += "<p>Registro: "+numRegistros+" Edad: "+txtEdad.value+" Altura: "+txtAltura.value+" Peso: "+txtPeso.value+" IMC: "+txtimc.value+" Nivel: "+txtNivel.value+"</p>";
}

function borrarRegistros() {
	imcTotal = 0;
	numRegistros = 0;
	registros.innerHTML = "";
	document.getElementById("promedioIMC").innerHTML = "";
}

